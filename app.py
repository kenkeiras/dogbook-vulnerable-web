#!/usr/bin/env python3

import os
import traceback
import sqlite3

from flask import Flask, render_template, send_file, request, redirect, Response
app = Flask(__name__)

## Login methods
# Get the login page
@app.route("/login")
def login():
    return render_template('login.html',
                           retry=request.args.get('retry'),
                           ready=request.args.get('ready'))

# Perform login
@app.route("/do-login", methods=["POST"])
def do_login():
    username = request.form.get('username')
    password = request.form.get('password')

    ## Check username on DB
    # Build query
    query = ('''
    SELECT id
    FROM users
    where username=\'{}\'
    AND   password=\'{}\'
    '''.format(username, password))

    print("------- 8< ---------")
    print(query.strip())
    print("------- >8 ---------")
    
    db = sqlite3.connect('db.sqlite3')
    cursor = db.cursor()
    result, error = 0, False
    try:
        cursor.execute(query)
        query_result = cursor.fetchall()
        if len(query_result) == 0:
            result = None
        else:
            result = query_result[0][0]
    except:
        error = traceback.format_exc()

    cursor.close()
    db.close()
    
    # Return results
    if error: # If there was an error, be helpful :)
        return Response(error, mimetype='text/plain')
    elif result is None: # No results, the login failed
        return redirect('/login?retry=yes')
    else: # Result found, log-in!
        response = redirect('/')
        response.set_cookie('user_id', str(result).encode())
        return response

# Logout
@app.route("/logout")
def logout():
    response = redirect('/')
    response.delete_cookie('user_id')
    return response

## Register methods
# Get the registration page
@app.route("/register")
def register():
    return render_template('register.html',
                           retry=request.args.get('retry'))


# Perform registration
@app.route("/do-register", methods=["POST"])
def do_register():
    username = request.form.get('username')
    password = request.form.get('password')
    ## Check username on DB
    # Build query
    query = ('''
    INSERT INTO users (username, password)
    VALUES ('{}', '{}')
    '''.format(username, password))

    print("------- 8< ---------")
    print(query.strip())
    print("------- >8 ---------")
    
    db = sqlite3.connect('db.sqlite3')
    cursor = db.cursor()
    error = False
    try:
        cursor.execute(query)
    except:
        error = traceback.format_exc()

    cursor.close()
    db.commit()
    db.close()
    
    # Return results
    if error: # If there was an error, probably registration failed :P
        return redirect('/register?retry=yes')
    else:
        return redirect('/login?ready=yes')

## Functionality
# Index page
@app.route('/')
def index():
    user_id = request.cookies.get('user_id')
    query = ('''
    SELECT bark.content, user.id, user.username
    FROM barks bark
    JOIN users user
    ON bark.sender = user.id
    ORDER BY bark.id desc
    LIMIT 20 -- Limit to 20 barks
    ''')

    print("------- 8< ---------")
    print(query.strip())
    print("------- >8 ---------")
    
    db = sqlite3.connect('db.sqlite3')
    cursor = db.cursor()
    cursor.execute(query)
    results = cursor.fetchall()

    cursor.close()
    db.close()
    
    return render_template('/index.html',
                           user_id=user_id,
                           barks=[
                               {
                                   "content": bark[0],
                                   "username": bark[2],
                               }
                               for bark
                               in results
                           ])

# Send a bark
@app.route('/send-bark', methods=["POST"])
def send_bark():
    user_id = request.cookies.get('user_id')
    if user_id is None:
        return redirect('/?retry=yes')
    
    bark = request.form.get('bark')

    query = ('''
    INSERT INTO barks (sender, content)
    VALUES ({}, '{}')
    '''.format(user_id, bark))

    print("------- 8< ---------")
    print(query.strip())
    print("------- >8 ---------")
    
    db = sqlite3.connect('db.sqlite3')
    cursor = db.cursor()
    error = False
    try:
        cursor.execute(query)
    except:
        error = traceback.format_exc()

    cursor.close()
    db.commit()
    db.close()
    if error: # If there was an error, be helpful :)
        return Response(error, mimetype='text/plain')
    else: # Barking worked, back to index
        response = redirect('/')
        return response

## Other methods
# Get static files
@app.route("/static/<filename>")
def static_file(filename):
    directory = os.path.abspath(os.path.dirname(__file__))
    return send_file(os.path.join(directory, 'static', filename))

## Initialization
def initialize_db():
    db = sqlite3.connect('db.sqlite3')
    cursor = db.cursor()
    print(" X Initializing DB...")
    try:
        cursor.execute('''CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username text UNIQUE,
        password text
        );
        ''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS barks (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        sender INTEGER,
        content text,
        FOREIGN KEY (sender) REFERENCES users(id)
        );
        ''')
    except Exception as e:
        traceback.print_exc()
    cursor.close()
    db.close()
    

initialize_db()

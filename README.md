# Dogbook

A very broken website to test web vulnerabilities.

## Usage

* Get dependencies: `pip3 install -r requirements.txt`
* Run: `FLASK_DEBUG=1 flask run`
